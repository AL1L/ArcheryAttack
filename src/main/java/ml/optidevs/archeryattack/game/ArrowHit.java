package ml.optidevs.archeryattack.game;

import ml.optidevs.archeryattack.Main;
import org.bukkit.*;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.block.Chest;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Collection;

public class ArrowHit {
	Arrow a = null;
	Player p = null;
	EntityDamageByEntityEvent e = null;
	Chest c;

	public ArrowHit(Player player, Arrow arrow) {
		a = arrow;
		p = player;
	}

	public ArrowHit(Player player, Arrow arrow, EntityDamageByEntityEvent event) {
		a = arrow;
		p = player;
		e = event;

	}

	public ArrowHit(Player player, Arrow arrow, Chest chest) {
		a = arrow;
		p = player;
		c = chest;

	}

	public ArrowHit heal() {
		if (!p.isInvulnerable()) {
			p.addPotionEffect(new PotionEffect(PotionEffectType.ABSORPTION, 20 * 60, 4, true), true);
			// p.setMaxHealth(20);
			p.setHealth(p.getMaxHealth());
			p.sendMessage(ChatColor.AQUA + "You were healed by " + ChatColor.LIGHT_PURPLE + a.getCustomName()
					+ ChatColor.AQUA + "!");
			Actions.shootFireWork(p.getLocation(), Type.BURST, Color.WHITE, Color.FUCHSIA, Color.AQUA, Color.WHITE);
		} else {
			Actions.shootFireWork(p.getLocation(), Type.BURST, Color.RED, Color.FUCHSIA, Color.AQUA, Color.WHITE);
		}
		p.damage(-1);
		trySetCancelled(true);
		return this;
	}

	public ArrowHit angel() {
		p.damage(-1);
		p.sendMessage(ChatColor.LIGHT_PURPLE + "You were hit with an angel arrow by " + ChatColor.DARK_PURPLE
				+ a.getCustomName() + ChatColor.LIGHT_PURPLE + "!\n\nPress " + ChatColor.DARK_PURPLE + "Shift"
				+ ChatColor.LIGHT_PURPLE + " + " + ChatColor.DARK_PURPLE + "Left Click " + ChatColor.LIGHT_PURPLE
				+ "to become human agian.");
		Actions.shootFireWork(p.getLocation(), Type.BURST, Color.FUCHSIA, Color.PURPLE, Color.ORANGE);
		trySetCancelled(true);
		p.addPotionEffect(new PotionEffect(PotionEffectType.LEVITATION, 15 * 20, 1));
		p.setInvulnerable(true);
		new BukkitRunnable() {
			int grace = 3 * 20;
			boolean warn = true;
			boolean once = true;
			ItemStack chestplate = p.getInventory().getChestplate();

			public void run() {
				if (once) {
					p.getInventory().setChestplate(new ItemStack(Material.ELYTRA));
					once = false;
				}
				Collection<PotionEffect> ps = p.getActivePotionEffects();
				int time = 15;
				for (PotionEffect p : ps) {
					if (p.getType().equals(PotionEffectType.LEVITATION)) {
						time = p.getDuration() / 20;
					}
				}
				if (p.hasPotionEffect(PotionEffectType.LEVITATION)) {
					time = time + 1;
					Actions.sendActionbar(p, "&5&lTime left: &d&l&n" + time);
				}
				if (!p.hasPotionEffect(PotionEffectType.LEVITATION)) {
					if (warn) {
						p.sendMessage("You will become vulnerable agian in 3 seconds!");
						Actions.sendActionbar(p, "");
						warn = false;
					}

					Actions.sendActionbar(p, "&5Vulnerable in: &d&l&n" + (grace / 20 + 1));
					grace = grace - 1;
					if (grace < 0) {
						Actions.sendActionbar(p, "");
						p.getInventory().setChestplate(chestplate);
						p.setInvulnerable(false);
						this.cancel();
						p.sendMessage("You are now vulnerable!");
					}
				} else {
					grace = 3 * 20;
					warn = true;
				}
			}
		}.runTaskTimer(Main.gi(), 0, 1);
		return this;
	}

	public ArrowHit nromal() {
		if (!p.isInvulnerable())
			p.damage(20);
		if (p.getHealth() == 0) {
			Actions.shootFireWork(p.getLocation(), Type.STAR, Color.BLACK, Color.RED, Color.MAROON);
		} else {
			Actions.shootFireWork(p.getLocation(), Type.STAR, Color.LIME, Color.WHITE, Color.RED);
		}
		return this;
	}

	public ArrowHit sniper() {
		if (!p.isInvulnerable())
			if (a.hasGravity()) {
				p.damage(20);
			} else {
				p.damage(40);
			}
		if (p.getHealth() == 0) {
			Actions.shootFireWork(p.getLocation(), Type.CREEPER, Color.BLACK, Color.RED, Color.MAROON);
		} else {
			Actions.shootFireWork(p.getLocation(), Type.CREEPER, Color.LIME, Color.WHITE, Color.RED);
		}
		return this;
	}

	public ArrowHit bounce() {
		if (!p.isInvulnerable())
			p.damage(15);
		if (p.getHealth() == 0) {
			Actions.shootFireWork(p.getLocation(), Type.BALL, Color.BLACK, Color.RED, Color.MAROON);
		} else {
			Actions.shootFireWork(p.getLocation(), Type.BALL, Color.LIME, Color.WHITE, Color.RED);
		}
		return this;
	}

	public ArrowHit treasureChest() {
		Actions.shootFireWork(c.getLocation(), Type.BALL, Color.SILVER, Color.OLIVE, Color.YELLOW);
		c.getLocation().getWorld().spawnParticle(Particle.EXPLOSION_HUGE, c.getLocation(), 1, 0,0,0);
		c.setType(Material.AIR);
		Actions.givePlayerTCReward(p);
		if (p.getGameMode().equals(GameMode.SURVIVAL) || p.getGameMode().equals(GameMode.ADVENTURE))
			p.getInventory().addItem(new ItemStack(Material.ARROW, 1));
		return this;
	}

	public ArrowHit lastly() {
		a.remove();
		if (c != null) {
			c.getBlock().setType(Material.AIR);
		}
		if (e != null) {
			if (p.getHealth() == 0 || p.isDead()) {
				Actions.playerKilledPlayer(Bukkit.getPlayer(a.getName()), (Player) e.getEntity(), "none");
			}
		}
		return this;
	}

	public ArrowHit removeChest() {
		c.getBlock().setType(Material.AIR);
		return this;
	}

	private ArrowHit trySetCancelled(boolean cancel) {
		if (e != null) {
			e.setCancelled(true);
		}
		return this;
	}
}
