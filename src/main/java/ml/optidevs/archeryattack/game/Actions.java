package ml.optidevs.archeryattack.game;

import ml.optidevs.archeryattack.Main;
import net.minecraft.server.v1_14_R1.ChatMessageType;
import net.minecraft.server.v1_14_R1.IChatBaseComponent;
import net.minecraft.server.v1_14_R1.PacketPlayOutChat;
import org.bukkit.*;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.block.Chest;
import org.bukkit.craftbukkit.v1_14_R1.entity.CraftPlayer;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class Actions {
	public static Player topKiller = null;

	public static void setTopKiller(Player player) {
		if (player == null) {
			topKiller = null;
			return;
		}
		player.sendMessage("You are now the king!");
		topKiller = player;
		new BukkitRunnable() {
			double t = 0;
			double r = 0.30;

			public void run() {
				Location loc = player.getEyeLocation();
				t = t + Math.PI / 16;

				double x = r * Math.cos(t);
				double y = 0.3;
				double z = r * Math.sin(t);
				loc.add(x, y, z);
				Collection<? extends Player> oplys = Bukkit.getOnlinePlayers();
				// oplys.remove(player);
				loc.getWorld().spawnParticle(Particle.VILLAGER_HAPPY, loc, 1);
				loc.subtract(x, y, z);
				if (topKiller == null || topKiller != player) {
					this.cancel();
				}
			}
		}.runTaskTimer(Main.gi(), 0, 1);
	}

	public static ArrowHit newHit(Player player, Arrow arrow) {
		return new ArrowHit(player, arrow);
	}

	public static ArrowHit newHit(Player player, Arrow arrow, Chest chest) {
		return new ArrowHit(player, arrow, chest);
	}

	public static ArrowHit newHit(Player player, Arrow arrow, EntityDamageByEntityEvent event) {
		return new ArrowHit(player, arrow, event);
	}

	public static void shootFireWork(Location loc, Type type, Color fadeTo, Color... startColors) {
		Firework fw = (Firework) loc.getWorld().spawn(loc, Firework.class);
		FireworkMeta fm = fw.getFireworkMeta();
		fm.addEffect(FireworkEffect.builder().flicker(false).trail(false).with(type).withColor(startColors)
				.withFade(fadeTo).build());
		fm.setPower(0);
		fw.setFireworkMeta(fm);
	}

	public static void shootFireWork(Location loc, Type type, int power, Color fadeTo, Color... startColors) {
		Firework fw = (Firework) loc.getWorld().spawn(loc, Firework.class);
		FireworkMeta fm = fw.getFireworkMeta();
		fm.addEffect(FireworkEffect.builder().flicker(false).trail(false).with(type).withColor(startColors)
				.withFade(fadeTo).build());
		fm.setPower(power);
		fw.setFireworkMeta(fm);
	}

	public static List<Location> chests = new ArrayList<Location>();

	public static void tcPlaced(Chest chest) {
		Location loc = chest.getLocation();
		chests.add(loc);
		new BukkitRunnable() {
			public void run() {

				double x = 0.5;
				double y = 1;
				double z = 0.5;
				loc.add(x, y, z);

				loc.getWorld().spawnParticle(Particle.SPELL_MOB, loc, 0, 255 / 255D, 247 / 255D, 1 / 255D, 1);
				loc.getWorld().spawnParticle(Particle.SPELL_MOB, loc, 0, 255 / 255D, 165 / 255D, 1 / 255D, 1);
				// ParticleEffect.FIREWORKS_SPARK.send(Bukkit.getOnlinePlayers(),
				// loc, 0, 0, 0, 0.1, 1);
				loc.subtract(x, y, z);
				if (!loc.getBlock().getType().equals(Material.CHEST) || !loc.getBlock().getChunk().isLoaded()) {
					chests.remove(loc);
					this.cancel();
				}
			}
		}.runTaskTimer(Main.gi(), 0, 1);
	}

	public static void sendActionbar(Player p, String message) {
		IChatBaseComponent icbc = IChatBaseComponent.ChatSerializer
				.a("{\"text\": \"" + ChatColor.translateAlternateColorCodes('&', message) + "\"}");
		PacketPlayOutChat bar = new PacketPlayOutChat(icbc, ChatMessageType.GAME_INFO);
		((CraftPlayer) p).getHandle().playerConnection.sendPacket(bar);
	}

	public static ItemStack createItem(Material mat, int amt, String name, String... lore) {
		ItemStack is = new ItemStack(mat, amt);
		ItemMeta isim = is.getItemMeta();
		isim.setDisplayName(name);
		isim.setLore(Arrays.asList(lore));
		is.setItemMeta(isim);
		return is;
	}

	public static ItemStack createItem(Material mat, int amt, ItemMeta itmmeta) {
		ItemStack is = new ItemStack(mat, amt);
		is.setItemMeta(itmmeta);
		return is;
	}

	public static void playerKilledPlayer(Player killer, Player killed, String... achivements) {
		killer.sendMessage("You killed " + killed.getName());
		killed.sendMessage("You were killed by " + killer.getName());
		ScoreboardManager manager = Bukkit.getScoreboardManager();
		Scoreboard board = manager.getNewScoreboard();

		Objective objective = board.registerNewObjective("at", "dummy");
		objective.setDisplaySlot(DisplaySlot.SIDEBAR);
		objective.setDisplayName("Kills");

		Score score = objective.getScore(ChatColor.GREEN + killer.getName() + ":");
		score.setScore(score.getScore() + 1);

		for (Player p : Bukkit.getOnlinePlayers()) {
			p.setScoreboard(board);
		}
	}

	public static void givePlayerTCReward(Player p) {
		p.sendMessage("You got a treasure!");
		ItemStack r = new ItemStack(Material.ARROW, 1);
		ItemMeta m = r.getItemMeta();
		m.setDisplayName(ChatColor.LIGHT_PURPLE + "Heal Arrow");
		r.setItemMeta(m);
		p.getInventory().addItem(r);
	}
}
