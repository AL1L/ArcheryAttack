package ml.optidevs.archeryattack.game;

import ml.optidevs.archeryattack.Main;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Random;
import java.util.UUID;

// NOT THREAD SAFE!
@SuppressWarnings("unused")
public class ArenaManager {
	// Singleton instance
	private static ArenaManager am;

	// Player data
	private HashMap<UUID, Location> locs = new HashMap<UUID, Location>();
	private HashMap<UUID, ItemStack[]> inv = new HashMap<UUID, ItemStack[]>();
	private HashMap<UUID, ItemStack[]> armor = new HashMap<UUID, ItemStack[]>();
	private HashMap<UUID, Float> xpp = new HashMap<UUID, Float>();
	private HashMap<UUID, Integer> xpl = new HashMap<UUID, Integer>();

	private HashMap<UUID, Arena> arenas = new HashMap<UUID, Arena>();

	private ArenaManager() {
	} // Prevent instantiation

	// Singleton accessor with lazy initialization
	public static ArenaManager getManager() {

		if (am == null)
			am = new ArenaManager();

		return am;
	}

	public Arena newArena(Location[] side1Spawn, Location[] side2Spawn) {
		UUID id = UUID.randomUUID();
		while (arenas.containsKey(id)) {
			id = UUID.randomUUID();
		}
		Arena a = new Arena(side1Spawn, side2Spawn, id);
		arenas.put(id, a);
		return a;
	}

	public void destroyArena(Arena arena) {
		UUID id = arena.getId();
		arenas.remove(id);
	}

	public void destroyArena(UUID id) {
		arenas.remove(id);
	}

	public Collection<Arena> getAllArenas() {
		return arenas.values();
	}

	public boolean arenaExists(UUID id) {
		return arenas.containsKey(id);
	}

	public Arena getArena(UUID id) {
		return arenas.get(id);
	}

	public Arena getArena(Player p) throws IOException {
		if (arenas.values().isEmpty())
			throw new IOException("No arenas found");
		for (Arena a : arenas.values()) {
			if (a.getPlayers().contains(p.getUniqueId())) {
				return a;
			}
		}
		throw new IOException("Player not found in arena");
	}

	public void addPlayer(Player p, UUID arena) throws IOException {
		UUID pid = p.getUniqueId();
		Arena a = Main.am.getArena(arena);
		locs.put(pid, p.getLocation());
		inv.put(pid, p.getInventory().getContents());
		armor.put(pid, p.getInventory().getArmorContents());
		xpp.put(pid, p.getExp());
		xpl.put(pid, p.getExpToLevel());

		p.getInventory().setArmorContents(null);
		p.getInventory().clear();
		p.setExp(0);
		p.setTotalExperience(0);

		a.addPlayer(p.getUniqueId());
		a.setPlayerTeam(p, new Random().nextInt(1) + 1);
		Location[] s = a.getSpawns(a.getPlayerTeam(pid));
		int i = new Random().nextInt(s.length);
		Location l = s[i];
		l.setPitch(0);
		l.setY(0);
		p.teleport(l);
	}

	public void removePlayer(Player p) throws IOException {
		UUID pid = p.getUniqueId();
		Arena a = Main.am.getArena(p);
		if (armor.get(pid).length != 0 || armor.get(pid) != null)
			p.getInventory().setArmorContents(armor.get(pid));
		if (inv.get(pid).length != 0 || inv.get(pid) != null)
			p.getInventory().addItem(inv.get(pid));
		p.setExp(xpp.get(pid));
		p.setTotalExperience(xpl.get(pid));
		p.teleport(locs.get(pid));

		a.removePlayer(p.getUniqueId());

	}

	public void tpPlayerToSpawn(Player p) throws IOException {
		UUID pid = p.getUniqueId();
		Arena a = Main.am.getArena(pid);
		Location[] s = a.getSpawns(a.getPlayerTeam(pid));
		int i = new Random().nextInt(s.length);
		Location l = s[i];
		l.setPitch(0);
		l.setY(0);
		p.teleport(l);

	}

	public boolean inGame(Player p) {
		if (arenas.values().isEmpty())
			return false;
		for (Arena a : arenas.values()) {
			if (a.getPlayers().contains(p.getUniqueId())) {
				return true;
			}
		}
		return false;
	}

	// UTILITY METHODS
	public String serializeLoc(Location l) {
		return l.getWorld().getName() + "," + l.getBlockX() + "," + l.getBlockY() + "," + l.getBlockZ();
	}

	public Location deserializeLoc(String s) {
		String[] st = s.split(",");
		return new Location(Bukkit.getWorld(st[0]), Integer.parseInt(st[1]), Integer.parseInt(st[2]),
				Integer.parseInt(st[3]));
	}
}
