package ml.optidevs.archeryattack.game;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

// Also NOT thread safe
public class Arena {
	// Individual arena info here

	private final UUID i;
	private Location[] s1;
	private Location[] s2;
	private final List<UUID> p = new ArrayList<UUID>();
	private HashMap<UUID, Integer> pt = new HashMap<UUID, Integer>();
	private HashMap<UUID, Integer> k = new HashMap<UUID, Integer>();
	private long t;

	public Arena(Location[] side1Spawn, Location[] side2Spawn, UUID id) {
		s1 = side1Spawn;
		s2 = side2Spawn;
		i = id;
		t = System.currentTimeMillis();
	}

	// Getters

	/**
	 * Gets the ID of this game session.
	 * 
	 * @return ID of the game
	 */
	public UUID getId() {
		return i;
	}

	/**
	 * Gets the start time of the game
	 * 
	 * @return Start time of the game in milliseconds
	 */
	public long getStartTime() {
		return t;
	}

	/**
	 * Gets the list of all registered players to this game
	 * 
	 * @return List of every player in the game in UUID format
	 */
	public List<UUID> getPlayers() {
		return p;
	}

	/**
	 * Sets the amount of kills a player has
	 * 
	 * @param player
	 *            UUID of the player you want to set
	 * 
	 * @param amount
	 *            What to set their kills to
	 * 
	 * @see #getPlayerKills(UUID)
	 */
	public void setPlayerKills(UUID player, int amount) {
		k.put(player, amount);
	}

	/**
	 * Gets a players kill count
	 * 
	 * @param player
	 *            UUID of the player you want to get
	 * 
	 * @return Number of kills a player has
	 * 
	 * @see #setPlayerKills(UUID, int)
	 */
	public int getPlayerKills(UUID player) {
		return k.get(player);
	}

	/**
	 * Gets the spawns for a specific side <em>(or team)</em>
	 * 
	 * @param side
	 *            Side of the spawns you want to get<br>
	 *            <em>Can be either <strong>1</strong> or <strong>2</strong></em>
	 * 
	 * @return Array of locations defining where spawns are
	 * 
	 * @throws IOException
	 */
	public Location[] getSpawns(int side) throws IOException {
		if (side == 1) {
			return s1;
		} else if (side == 2) {
			return s2;
		} else {
			throw new IOException("Invalid side id");
		}
	}

	public void addPlayer(Player p) {
		this.p.add(p.getUniqueId());
	}

	public void addPlayer(UUID uuid) {
		this.p.add(uuid);
	}

	public void removePlayer(Player p) {
		this.p.remove(p.getUniqueId());
	}

	public void removePlayer(UUID uuid) {
		this.p.remove(uuid);
	}

	public void setPlayerTeam(UUID uuid, int side) {
		pt.put(uuid, side);
	}

	public void setPlayerTeam(Player p, int side) {
		pt.put(p.getUniqueId(), side);
	}

	public int getPlayerTeam(UUID uuid) {
		return pt.get(uuid);
	}

	public int getPlayerTeam(Player p) {
		return pt.get(p.getUniqueId());
	}
}
