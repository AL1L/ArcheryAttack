package ml.optidevs.archeryattack.game;

import ml.optidevs.archeryattack.Main;
import org.bukkit.*;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Chest;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.*;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.BlockIterator;
import org.bukkit.util.Vector;
import org.spigotmc.event.entity.EntityDismountEvent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Mechanics implements Listener {
	@EventHandler
	public void toggle(PlayerInteractEvent event) {
		boolean stop = true;
		if (stop)
			return;
		final Player p = event.getPlayer();
		if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
			new BukkitRunnable() {
				double t = 0;
				double r = 0.30;

				public void run() {
					Location loc = p.getEyeLocation();
					t = t + Math.PI / 16;

					double x = r * Math.cos(t);
					double y = 0.3;
					double z = r * Math.sin(t);
					loc.add(x, y, z);
					loc.getWorld().spawnParticle(Particle.VILLAGER_HAPPY, loc, 1, 0, 0, 0);
					loc.subtract(x, y, z);
					if (t > Math.PI * 8) {
						this.cancel();
					}
				}
			}.runTaskTimer(Main.gi(), 0, 1);
		}
	}

	public ArrayList<Projectile> arrows = new ArrayList<Projectile>();

	@EventHandler
	public void arrowShoot(EntityShootBowEvent e) {
		if (e.getEntity() instanceof Player) {
			// Player p = (Player) e.getEntity();
			ItemMeta bow = e.getBow().getItemMeta();
			Arrow arrow = (Arrow) e.getProjectile();
			e.getProjectile().setCustomName(e.getEntity().getName());
			String bowType = "none";
			if (!bow.hasLore()) {
				bowType = "none";
			} else {
				String type = bow.getLore().get(1);
				if (type.contains("Heal")) {
					e.getBow().setDurability((short) -1);
					bowType = "heal";
					arrow.setKnockbackStrength(0);
				} else if (type.contains("Normal")) {
					e.getBow().setDurability((short) -1);
					bowType = "normal";
					arrow.setCritical(false);
				} else if (type.contains("Sniper")) {
					e.getBow().setDurability((short) -1);
					bowType = "sniper";
					arrow.setKnockbackStrength(2);
					if (arrow.isCritical()) {
						arrow.setCritical(false);
						arrow.setGravity(false);
						new BukkitRunnable() {
							double secontsToLive = 3.5;

							public void run() {
								int life = arrow.getTicksLived();

								if (life > secontsToLive * 20)
									if (!arrow.isDead())
										arrow.remove();
							}
						}.runTaskTimer(Main.gi(), 0, 1);
					}
				} else if (type.contains("Angular")) {
					e.getBow().setDurability((short) -1);
					bowType = "bounce";
					arrow.setBounce(true);
					arrow.setKnockbackStrength(0);
					arrow.setCritical(false);
					final int MAX_BOUNCE_COUNT = 4;

					final int bouncingCount = Math.max(1, MAX_BOUNCE_COUNT);
					arrow.setMetadata("bouncing", new FixedMetadataValue(Main.gi(), bouncingCount));
				} else if (type.contains("Angel")) {
					e.getBow().setDurability((short) -1);
					bowType = "angel";
					arrow.setKnockbackStrength(0);
					arrow.setCritical(false);
				} else {
					bowType = "none";
				}
			}
			e.getProjectile().setCustomNameVisible(false);
			e.getProjectile().setMetadata("BOWTYPE", new FixedMetadataValue(Main.gi(), bowType));
			if (bowType == "none")
				return;
			new BukkitRunnable() {

				public void run() {
					if (arrow.isOnGround() || !arrow.isValid()) {
						this.cancel();
					}
					Location loc = arrow.getLocation();
					loc.add(0, 0, 0);
					loc.getWorld().spawnParticle(Particle.FIREWORKS_SPARK, loc, 0, 0, 0, 0);

				}
			}.runTaskTimer(Main.gi(), 0, 1);

		} else {
			e.getProjectile().setCustomName("__");
			e.getProjectile().setCustomNameVisible(true);
		}

	}

	@EventHandler
	public void arrowHitEvent(ProjectileHitEvent event) {
		if (!(event.getEntity() instanceof Arrow))
			return;

		Arrow arrow = (Arrow) event.getEntity();
		String bowtype = arrow.getMetadata("BOWTYPE").get(0).asString();
		if (bowtype == "none") {
			return;
		}
		Player p = (Player) arrow.getShooter();
		World world = arrow.getWorld();
		BlockIterator bi = new BlockIterator(world, arrow.getLocation().toVector(), arrow.getVelocity().normalize(), 0,
				4);
		Block hit = null;

		while (bi.hasNext()) {
			hit = bi.next();
			if (hit.getType() != Material.AIR) {
				break;
			}
		}
		if (hit.getType().equals(Material.CHEST)) {
			if (hit.getState() instanceof Chest) {
				Chest chest = (Chest) hit.getState();
				if (ChatColor.stripColor(chest.getCustomName()).equalsIgnoreCase("TCAT3752")) {
					Actions.newHit(p, arrow, (Chest) hit.getState()).treasureChest().lastly();
				}
			}
		}
		if (bowtype == "bounce") {
			arrow.remove();
			final double MIN_MAGNITUDE_THRESHOLD = 0.6;
			final int MAX_BOUNCE_COUNT = 4;

			Vector arrowVector = arrow.getVelocity();

			final double magnitude = Math.sqrt(Math.pow(arrowVector.getX(), 2) + Math.pow(arrowVector.getY(), 2)
					+ Math.pow(arrowVector.getZ(), 2));

			if (magnitude < MIN_MAGNITUDE_THRESHOLD) {
				return;
			}

			Location hitLoc = arrow.getLocation();

			BlockIterator b = new BlockIterator(hitLoc.getWorld(), hitLoc.toVector(), arrowVector, 0, 3);

			Block hitBlock = event.getEntity().getLocation().getBlock();

			Block blockBefore = hitBlock;
			Block nextBlock = b.next();

			while (b.hasNext() && nextBlock.getType() == Material.AIR) {
				blockBefore = nextBlock;
				nextBlock = b.next();
			}

			BlockFace blockFace = nextBlock.getFace(blockBefore);

			if (blockFace != null) {

				// Convert blockFace SELF to UP:
				if (blockFace == BlockFace.SELF) {
					blockFace = BlockFace.UP;
				}

				Vector hitPlain = new Vector(blockFace.getModX(), blockFace.getModY(), blockFace.getModZ());

				double dotProduct = arrowVector.dot(hitPlain);
				Vector u = hitPlain.multiply(dotProduct).multiply(2.0);

				float speed = (float) magnitude;
				speed *= 0.6F;

				Arrow newArrow = arrow.getWorld().spawnArrow(arrow.getLocation(), arrowVector.subtract(u), speed,
						12.0F);
				newArrow.setMetadata("BOWTYPE", new FixedMetadataValue(Main.gi(), "bounce"));

				List<MetadataValue> metaDataValues = arrow.getMetadata("bouncing");
				if (metaDataValues.size() > 0) {
					int prevBouncingRate = metaDataValues.get(0).asInt();
					if (prevBouncingRate > MAX_BOUNCE_COUNT) {
						newArrow.setMetadata("bouncing", new FixedMetadataValue(Main.gi(), prevBouncingRate - 1));
					}
				}

				newArrow.setShooter(arrow.getShooter());
				newArrow.setFireTicks(arrow.getFireTicks());

				arrow.remove();
				new BukkitRunnable() {

					public void run() {
						if (newArrow.isOnGround() || !newArrow.isValid()) {
							this.cancel();
							// newArrow.remove();
						}
						Location loc = newArrow.getLocation();
						loc.add(0, 0, 0);
						loc.getWorld().spawnParticle(Particle.FIREWORKS_SPARK, loc, 0, 0, 0, 0);

					}
				}.runTaskTimer(Main.gi(), 0, 1);
				return;
			}
		}
		arrow.remove();
	}

	@EventHandler
	public void arrowItemSpawn(ItemSpawnEvent e) {
		ItemStack i = e.getEntity().getItemStack();
		if (i.getType() == Material.ARROW && i.getAmount() == 1) {
			e.setCancelled(true);
		}
	}

	HashMap<Player, Block> OpenTCs = new HashMap<Player, Block>();

	@EventHandler
	public void tcOpen(InventoryOpenEvent e) {
		InventoryHolder holder = e.getInventory().getHolder();
		if (!(holder instanceof Chest)) return;
		Chest chest = (Chest) holder;
		Inventory i = e.getInventory();
		Player p = (Player) e.getPlayer();
		if (ChatColor.stripColor(chest.getCustomName()).equalsIgnoreCase("TCAT3752")) {
			e.setCancelled(true);
			OpenTCs.put(p, i.getLocation().getBlock());
			// e.setCancelled(true);
			Inventory inv = Bukkit.createInventory(p, 9,
					ChatColor.translateAlternateColorCodes('&', "&cArchery Attack &6Treasure Chest"));
			ItemStack gp = new ItemStack(Material.LIGHT_GRAY_STAINED_GLASS_PANE, 1);
			ItemMeta gpim = gp.getItemMeta();
			gpim.setDisplayName(ChatColor.RED + "" + ChatColor.BOLD + ">" + ChatColor.GOLD + "" + ChatColor.BOLD + ">");
			gp.setItemMeta(gpim);
			inv.setItem(0, gp);
			inv.setItem(1, gp);
			inv.setItem(2, gp);
			inv.setItem(3, gp);
			gpim.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "<" + ChatColor.RED + "" + ChatColor.BOLD + "<");
			gp.setItemMeta(gpim);
			inv.setItem(5, gp);
			inv.setItem(6, gp);
			inv.setItem(7, gp);
			inv.setItem(8, gp);

			ItemStack ca = new ItemStack(Material.NETHER_STAR, 1);
			ItemMeta im = ca.getItemMeta();
			// String[] lore = { null };
			im.setLore(null);
			im.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&bClick to accept &areward&b!"));
			im.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
			im.addItemFlags(ItemFlag.HIDE_DESTROYS);
			im.addItemFlags(ItemFlag.HIDE_ENCHANTS);
			im.addItemFlags(ItemFlag.HIDE_PLACED_ON);
			im.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
			im.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
			ca.setItemMeta(im);
			inv.setItem(4, ca);
			p.openInventory(inv);
		}

		boolean noGo = false;
		if (noGo) {
			Actions.sendActionbar((Player) e.getPlayer(),
					"&c&lThis is a Treasure Chest!&7&l Hit it with an arrow to collect reward.");
			e.getPlayer().getWorld().playSound(e.getPlayer().getLocation(), Sound.BLOCK_CHEST_LOCKED, 1, 1);
			e.setCancelled(true);
		}
	}

	@EventHandler
	public void tcClose(InventoryCloseEvent e) {
		InventoryHolder holder = e.getInventory().getHolder();
		if (!(holder instanceof Chest)) return;
		Chest chest = (Chest) holder;

		Player p = (Player) e.getPlayer();
		if (ChatColor.stripColor(chest.getCustomName()).equalsIgnoreCase("TCAT3752")) {
			OpenTCs.remove(p);
		}
	}

	@EventHandler
	public void tcPlaced(BlockPlaceEvent e) {
		Block b = e.getBlock();
		if (b.getState() instanceof Chest) {
			Chest c = (Chest) b.getState();

			if (c.getCustomName().equalsIgnoreCase("TCAT3752")) {
				Actions.tcPlaced(c);

			}
		}
	}

	@EventHandler
	public void arrowHitEntity(EntityDamageByEntityEvent event) {
		if (event.getDamager() instanceof Arrow) {
			if (event.getEntity() instanceof ArmorStand) {
				event.setCancelled(true);
				return;
			}
			if (event.getEntity() instanceof Player) {
				if (((Arrow) event.getDamager()).getMetadata("BOWTYPE").get(0).asString() == "none") {
					return;
				}
				Arrow arrow = (Arrow) event.getDamager();
				if (!(arrow.getCustomName().equals("__"))) {
					// Player s = Bukkit.getPlayer(arrow.getCustomName());
					// Player damager =
					// Bukkit.getPlayer(event.getDamager().getName());
					Player p = (Player) event.getEntity();
					event.setCancelled(true);
					// Bow types
					String bowtype = arrow.getMetadata("BOWTYPE").get(0).asString();
					if (bowtype == "normal") {
						Actions.newHit(p, arrow, event).nromal().lastly();
					} else if (bowtype == "heal") {
						Actions.newHit(p, arrow, event).heal().lastly();
					} else if (bowtype == "sniper") {
						Actions.newHit(p, arrow, event).sniper().lastly();
					} else if (bowtype == "bounce") {
						Actions.newHit(p, arrow, event).bounce().lastly();
					} else if (bowtype == "angel") {
						Actions.newHit(p, arrow, event).angel().lastly();
					}
				}

			}
		}

	}

	@EventHandler
	public void playerKilled(PlayerDeathEvent e) {
		e.setDeathMessage(null);
		e.setDroppedExp(0);
	}

	@EventHandler
	public void stopAngel(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		if (p.hasPotionEffect(PotionEffectType.LEVITATION)) {
			if (p.isSneaking()) {
				if (e.getAction().equals(Action.LEFT_CLICK_AIR) || e.getAction().equals(Action.LEFT_CLICK_BLOCK)) {
					p.removePotionEffect(PotionEffectType.LEVITATION);
				}
			}
		}
	}

	@EventHandler
	public void invClick(InventoryClickEvent e) {
		InventoryHolder holder = e.getInventory().getHolder();
		if (!(holder instanceof Chest)) return;
		Chest chest = (Chest) holder;

		if (e.getSlotType() == SlotType.ARMOR) {
			Player p = (Player) e.getWhoClicked();
			if (p.isInvulnerable()) {
				e.setCancelled(true);
			}
		} else if (ChatColor.stripColor(chest.getCustomName())
				.equalsIgnoreCase("Archery Attack Treasure Chest")) {
			e.setCancelled(true);
			// Inventory inv = e.getClickedInventory();
			// ItemStack is = e.getCursor();
			Player p = (Player) e.getWhoClicked();
			int sn = e.getSlot();
			if (e.getSlotType() == SlotType.CONTAINER) {
				if (sn == 4) {
					p.closeInventory();
					Block b = OpenTCs.get(p);
					Actions.shootFireWork(b.getLocation(), Type.BALL, Color.SILVER, Color.OLIVE, Color.YELLOW);
					b.getLocation().getWorld().spawnParticle(Particle.EXPLOSION_HUGE, b.getLocation(), 1);
					b.setType(Material.AIR);
					Actions.givePlayerTCReward(p);

				}
			}
		}
	}

	@EventHandler
	public void batOff(EntityDismountEvent e) {
		if (e.getDismounted() instanceof Arrow) {
			if (e.getDismounted().getCustomName() != "__") {
				Bat b = (Bat) e.getEntity();
				b.remove();
				b.setHealth(0);
			}
		}

	}

	@EventHandler
	public void lookForTC(ChunkLoadEvent e) {
		Chunk c = e.getChunk();

		int xmax = 15;

		int ymax = 127;

		int zmax = 15;

		for (int x = 0; x < xmax; x++) {
			for (int y = 0; y < ymax; y++) {
				for (int z = 0; z < zmax; z++) {
					Block b = c.getBlock(x, y, z);
					// Bukkit.broadcastMessage("Checking: "+x+" "+y+" "+z+"");
					if (b.getState() instanceof Chest) {
						// Bukkit.broadcastMessage("Chest Found at: "+x+" "+y+"
						// "+z+"");
						Chest ch = (Chest) b.getState();

						if (ch.getCustomName() != null && ch.getCustomName().equalsIgnoreCase("TCAT3752"))
							Actions.tcPlaced(ch);
					}
				}
			}
		}
	}

	@EventHandler
	public void chat(AsyncPlayerChatEvent e) {
		e.setMessage(ChatColor.translateAlternateColorCodes('&', e.getMessage()));
		e.setFormat(ChatColor.GOLD + "%s " + ChatColor.DARK_GRAY + "" + ChatColor.BOLD + "\u00bb " + ChatColor.RESET
				+ "%s");
	}
}
