package ml.optidevs.archeryattack;

import ml.optidevs.archeryattack.game.Actions;
import ml.optidevs.archeryattack.game.Arena;
import ml.optidevs.archeryattack.game.ArenaManager;
import ml.optidevs.archeryattack.game.Mechanics;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class Main extends JavaPlugin {

	public ConsoleCommandSender Logger = getServer().getConsoleSender();
	public PluginDescriptionFile desc = getDescription();
	public static YamlConfiguration LANG;
	public static File LANG_FILE;
	public static ArenaManager am;

	@Override
	public void onEnable() {
		loadConfig();
		getServer().getPluginManager().registerEvents(new Signs(), this);
		getServer().getPluginManager().registerEvents(new Mechanics(), this);
		Logger.sendMessage("[ArcheryAttack] Plugin Enabled " + desc.getVersion());
		// Bukkit.dispatchCommand(getServer().getConsoleSender(), "archeryattack
		// chests");
		am = ArenaManager.getManager();
	}

	// @Override
	public void onDisable() {
		Logger.sendMessage("[ArcheryAttack] Plugin Disabled " + desc.getVersion());

	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (args.length == 0) {
			sender.sendMessage("Help will be here soon!");

		} else if (args[0].equalsIgnoreCase("tk")) {
			if (!sender.isOp())
				return true;
			if (args.length != 2) {
				sender.sendMessage("Invilic args");
			} else {
				if (args[1].equalsIgnoreCase("none") || args[1].equalsIgnoreCase("null")) {
					Actions.setTopKiller(null);
					return true;
				}
				try {
					Actions.setTopKiller(Bukkit.getPlayer(args[1]));
					Bukkit.broadcastMessage(Bukkit.getPlayer(args[1]).getName() + " is now the king of kills!");
				} catch (Exception e) {
					sender.sendMessage("Player not found!");
				}

				return true;
			}
		} else if (args[0].equalsIgnoreCase("chests")) {
			if (!sender.isOp())
				return true;
			int found = 0;
			if (!(sender instanceof Player)) {
				sender.sendMessage(ChatColor.AQUA + "Searching server for chests...");
				for (World w : Bukkit.getWorlds()) {
					for (Chunk c : w.getLoadedChunks()) {
						int xmax = 15;

						int ymax = 127;

						int zmax = 15;

						for (int x = 0; x < xmax; x++) {
							for (int y = 0; y < ymax; y++) {
								for (int z = 0; z < zmax; z++) {
									Block b = c.getBlock(x, y, z);
									// Bukkit.broadcastMessage("Checking: "+x+"
									// "+y+"
									// "+z+"");
									if (b.getState() instanceof Chest) {
										// Bukkit.broadcastMessage("Chest Found
										// at:
										// "+x+" "+y+"
										// "+z+"");
										Chest ch = (Chest) b.getState();

										if (ch.getCustomName().equalsIgnoreCase("TCAT3752")) {
											if (!Actions.chests.contains(b.getLocation()))
												Actions.tcPlaced(ch);
											found++;
										}
									}
								}
							}
						}
					}
				}
				sender.sendMessage(ChatColor.AQUA + "Found a total of " + found + " chests.");
				return true;
			}
			Player p = (Player) sender;
			Chunk c = p.getLocation().getChunk();
			p.sendMessage(ChatColor.GREEN + "Checking chunk " + c.getX() + ";" + c.getZ() + "...");
			int xmax = 15;

			int ymax = 127;

			int zmax = 15;

			for (int x = 0; x < xmax; x++) {
				for (int y = 0; y < ymax; y++) {
					for (int z = 0; z < zmax; z++) {
						Block b = c.getBlock(x, y, z);
						// Bukkit.broadcastMessage("Checking: "+x+" "+y+"
						// "+z+"");
						if (b.getState() instanceof Chest) {
							// Bukkit.broadcastMessage("Chest Found at:
							// "+x+" "+y+"
							// "+z+"");
							Chest ch = (Chest) b.getState();
							Location loc = b.getLocation();

							if (ch.getCustomName() != null && ch.getCustomName().equalsIgnoreCase("TCAT3752")) {
								if (!Actions.chests.contains(loc))
									Actions.tcPlaced(ch);
								found++;
							}
						}
					}
				}
			}
			p.sendMessage(ChatColor.GREEN + "Found a total of " + found + " chests.");
		} else if (args[0].equalsIgnoreCase("help")) {
			sender.sendMessage("Help will be here soon!");
		} else if (args[0].equalsIgnoreCase("join")) {
			if (!(sender instanceof Player)) {
				sender.sendMessage("Only players can run this command!");
			}
			Player p = (Player) sender;
			UUID id = UUID.randomUUID();
			try {

				id = UUID.fromString(args[1]);
			} catch (NumberFormatException e) {
				p.sendMessage("Invalid arena ID");
				return true;
			}
			if (!am.arenaExists(id)) {
				p.sendMessage("Arena doesn't exist");
				return true;
			}
			if(am.inGame(p)){
				p.sendMessage("You are already in a game!");
				return true;
			}
			try {
				am.addPlayer(p, id);
				p.sendMessage("Joined game " + id);
			} catch (IOException e) {
				p.sendMessage("Couldn't join game");
			}

		} else if (args[0].equalsIgnoreCase("create")) {
			if (!(sender instanceof Player)) {
				sender.sendMessage("Only players can run this command!");
			}
			Player p = (Player) sender;
			Arena a = am.newArena(Main.getConfigLocations("Arenas.TDM.Spawns.1"),
					Main.getConfigLocations("Arenas.TDM.Spawns.2"));
			p.sendMessage("Created arena " + a.getId().toString());
		} else if (args[0].equalsIgnoreCase("leave")) {
			if (!(sender instanceof Player)) {
				sender.sendMessage("Only players can run this command!");
			}
			Player p = (Player) sender;
			try {
				am.removePlayer(p);
				p.sendMessage("LEFT GAME");
			} catch (IOException e) {
				e.printStackTrace();
				p.sendMessage("You are not in a game");
			}

		} else if (args[0].equalsIgnoreCase("getGame")) {
			if (!(sender instanceof Player)) {
				sender.sendMessage("Only players can run this command!");
			}
			Player p = (Player) sender;
			if (args.length == 1) {
				try {
					Arena a = am.getArena(p);
					String players = "[";
					for (UUID id : a.getPlayers()) {
						Player s = Bukkit.getPlayer(id);
						players = players + s.getName() + ", ";
					}
					players = players.trim().substring(0, players.length() - 1) + "]";
					String s1 = "[";
					for (Location id : a.getSpawns(1)) {
						players = players + am.serializeLoc(id) + ":: ";
					}
					s1 = s1.trim().substring(0, s1.length() - 1) + "]";
					String s2 = "[";
					for (Location id : a.getSpawns(2)) {
						players = players + am.serializeLoc(id) + ":: ";
					}
					s2 = s2.trim().substring(0, s2.length() - 1) + "]";
					p.sendMessage("ArenaID: " + a.getId());
					p.sendMessage("Game Start: " + a.getStartTime());
					p.sendMessage("Players: " + players.trim());
					p.sendMessage("Spawns: ");
					p.sendMessage("  T1: " + s1);
					p.sendMessage("  T2: " + s2);
					p.sendMessage("Your Stats:");
					p.sendMessage("  Kills: " + a.getPlayerKills(p.getUniqueId()));
					return true;
				} catch (IOException e) {
					e.printStackTrace();
					p.sendMessage("You are not in a game");
					return true;
				}
			} else if (args.length == 2) {
				try {
					Arena a = am.getArena(UUID.fromString(args[1]));
					String players = "[";
					for (UUID id : a.getPlayers()) {
						Player s = Bukkit.getPlayer(id);
						players = players + s.getName() + ", ";
					}
					players = players.trim().substring(0, players.length() - 1) + "]";
					String s1 = "[";
					for (Location id : a.getSpawns(1)) {
						s1 = s1 + am.serializeLoc(id) + ":: ";
					}
					s1 = s1.trim().substring(0, s1.length() - 1) + "]";
					String s2 = "[";
					for (Location id : a.getSpawns(2)) {
						s2 = s2 + am.serializeLoc(id) + ":: ";
					}
					s2 = s2.trim().substring(0, s2.length() - 1) + "]";
					p.sendMessage("ArenaID: " + a.getId());
					p.sendMessage("Game Start: " + a.getStartTime());
					p.sendMessage("Players: " + players);
					p.sendMessage("Spawns: ");
					p.sendMessage("  T1: " + s1);
					p.sendMessage("  T2: " + s2);
				} catch (IOException e) {
					e.printStackTrace();
					return false;
				}
			}
		} else if (args[0].equalsIgnoreCase("fixme")) {
			if (!(sender instanceof Player)) {
				sender.sendMessage("Only players can run this command!");
			}
			Player p = (Player) sender;
			p.setInvulnerable(false);
			p.getInventory().setChestplate(null);
		} else if (args[0].equalsIgnoreCase("getbow")) {
			if (!(sender instanceof Player)) {
				sender.sendMessage("Only players can run this command!");
			}
			Player p = (Player) sender;
			// /at getbow <type>
			if (p.isOp()) {
				if (args.length == 2) {
					if (args[1].equalsIgnoreCase("heal")) {
						ItemStack bow = Actions.createItem(Material.BOW, 1,
								ChatColor.LIGHT_PURPLE + "Heal " + ChatColor.LIGHT_PURPLE + "Bow",
								ChatColor.DARK_GRAY + "" + ChatColor.BOLD + "███████",
								ChatColor.GOLD + "Type: " + ChatColor.LIGHT_PURPLE + "Heal");
						ItemMeta im = bow.getItemMeta();
						bow.setDurability((short) -1);
						bow.setItemMeta(im);
						p.getInventory().addItem(bow);
						p.sendMessage(ChatColor.GOLD + "Given HEAL bow");
					} else if (args[1].equalsIgnoreCase("normal")) {
						ItemStack bow = Actions.createItem(Material.BOW, 1,
								ChatColor.RED + "Normal " + ChatColor.RED + "Bow",
								ChatColor.DARK_GRAY + "" + ChatColor.BOLD + "███████",
								ChatColor.GOLD + "Type: " + ChatColor.RED + "Normal");
						ItemMeta im = bow.getItemMeta();
						bow.setDurability((short) -1);
						bow.setItemMeta(im);
						p.getInventory().addItem(bow);
						p.sendMessage(ChatColor.GOLD + "Given NORMAL bow");
					} else if (args[1].equalsIgnoreCase("sniper")) {
						ItemStack bow = Actions.createItem(Material.BOW, 1,
								ChatColor.DARK_RED + "Sniper " + ChatColor.DARK_RED + "Bow",
								ChatColor.DARK_GRAY + "" + ChatColor.BOLD + "███████",
								ChatColor.GOLD + "Type: " + ChatColor.RED + "Sniper");
						ItemMeta im = bow.getItemMeta();
						bow.setDurability((short) -1);
						bow.setItemMeta(im);
						p.getInventory().addItem(bow);
						p.sendMessage(ChatColor.GOLD + "Given SNIPER bow");
					} else if (args[1].equalsIgnoreCase("angular") || args[1].equalsIgnoreCase("BOUNCE")) {
						ItemStack bow = Actions.createItem(Material.BOW, 1,
								ChatColor.YELLOW + "Angular " + ChatColor.YELLOW + "Bow",
								ChatColor.DARK_GRAY + "" + ChatColor.BOLD + "████████",
								ChatColor.GOLD + "Type: " + ChatColor.YELLOW + "Angular");
						ItemMeta im = bow.getItemMeta();
						bow.setDurability((short) -1);
						bow.setItemMeta(im);
						p.getInventory().addItem(bow);
						p.sendMessage(ChatColor.GOLD + "Given BOUNCE bow");
					} else if (args[1].equalsIgnoreCase("Angel")) {
						ItemStack bow = Actions.createItem(Material.BOW, 1,
								ChatColor.DARK_PURPLE + "Angel " + ChatColor.DARK_PURPLE + "Bow",
								ChatColor.DARK_GRAY + "" + ChatColor.BOLD + "███████",
								ChatColor.GOLD + "Type: " + ChatColor.DARK_PURPLE + "Angel");
						ItemMeta im = bow.getItemMeta();
						bow.setDurability((short) -1);
						bow.setItemMeta(im);
						p.getInventory().addItem(bow);
						p.sendMessage(ChatColor.GOLD + "Given ANGEL bow");
					} else if (args[1].equalsIgnoreCase("all")) {
						p.performCommand("at getbow normal");
						p.performCommand("at getbow heal");
						p.performCommand("at getbow angel");
						p.performCommand("at getbow sniper");
						p.performCommand("at getbow bounce");
					}
				}
			}
		} else {
			sender.sendMessage("Help will be here soon!");
		}

		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
		String[] list;

		if (args.length == 1) {
			if (sender.isOp()) {
				String[] l = { "help", "list", "join", "leave", "create", "fixme", "admin", "chests" };
				list = l;
			} else {
				String[] l = { "help", "list", "join", "leave", "create", "fixme" };
				list = l;
			}
		} else {
			String[] l = { "" };
			list = l;
		}
		return Arrays.asList(list);
	}

	public static Location[] getConfigLocations(String path) {
		FileConfiguration c = Main.gi().getConfig();
		List<String> list = c.getStringList(path);
		List<Location> llist = new ArrayList<Location>();
		for (String s : list) {
			Location l = am.deserializeLoc(s);
			llist.add(l);
		}
		return llist.toArray(new Location[0]);
	}

	public void loadConfig() {
		getConfig().getDefaults();
		saveDefaultConfig();
		reloadConfig();
	}

	public void loadLang() {
		File lang = new File(getDataFolder(), "lang.yml");
		if (!lang.exists()) {
			try {
				getDataFolder().mkdir();
				lang.createNewFile();
				InputStream defConfigStream = this.getResource("lang.yml");
				if (defConfigStream != null) {
					YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(LANG_FILE);
					defConfig.save(lang);
					Lang.setFile(defConfig);
					return;
				}
			} catch (IOException e) {
				Logger.sendMessage("ERROR [ArcheryAttack] Couldn't create language file.");
				Logger.sendMessage("ERROR [ArcheryAttack] This is a fatal error. Now disabling");
			}
		}
		YamlConfiguration conf = YamlConfiguration.loadConfiguration(lang);
		for (Lang item : Lang.values()) {
			if (conf.getString(item.getPath()) == null) {
				conf.set(item.getPath(), item.getDefault());
			}
		}
		Lang.setFile(conf);
		Main.LANG = conf;
		Main.LANG_FILE = lang;
		try {
			conf.save(getLangFile());
		} catch (IOException e) {
			e.printStackTrace();
			Logger.sendMessage("ERROR [ArcheryAttack] Failed to save lang.yml.");
			Logger.sendMessage("ERROR [ArcheryAttack] Report this stack trace to OptiDevs.");
		}
	}

	public YamlConfiguration getLang() {
		return LANG;
	}

	public File getLangFile() {
		return LANG_FILE;
	}

	public static Main gi() {
		return (Main) Bukkit.getServer().getPluginManager().getPlugin("ArcheryAttack");
	}
}
